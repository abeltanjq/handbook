---
title: "RubyMine"
---

Website: https://www.jetbrains.com/ruby/

Best for: editing Ruby or Rails applications, which can include Javascript/Typescript

Strengths:

- Code completion
- Easy refactoring
- Powerful navigation tools
- integrated git client
- testing and debugging

Designed specifically for Ruby/Rails, but it also supports other languages and
file formats. It's an integrated development environment that also helps with
testing and debugging.
