---
title: "Jetbrains IDEs"
no_list: true
---

## Sub-pages

See the following sub-pages for information on configuration and usage of
Jetbrains IDEs in general, and for specific usage of IDEs.

{{< subpages >}}

## Overview

Jetbrains offers a [suite of powerful integrated development environments(IDEs)](https://www.jetbrains.com/products/)
for all major software development ecosystems. Although they are separate applications, each IDE shares a common UI and controls, which allows you to easily switch between them without re-learning UX or keybindings.

## Licenses

We have a central account for managing licenses of JetBrains' products like RubyMine or GoLand.
If you want to use one of their products, please log an [Access Request](https://gitlab.com/gitlab-com/team-member-epics/access-requests) and select the best option for your situation (single user, bulk user, etc.) and, once approved by your manager, [activate a license for yourself](https://www.jetbrains.com/help/license-vault-cloud/Activating_a_license.html). Use the license server URL `https://gitlab.fls.jetbrains.com` when activating your IDE, and be sure to log in via Okta when prompted to authenticate. Make sure to use your company email address when creating your Jetbrains account.

If you have an Individual License acquired through your own means, it is suitable for [General commercial use](https://www.jetbrains.com/store/comparison.html#LicenseComparison) and you may use it. However, GitLab will not reimburse an Individual License, as Individual License cannot be purchased or reimbursed by companies. That being said, even if you used to use an Individual License, you can always request a company issued one.

## Configuration and Setup

See the following resources for details on how to configure and setup JetBrains IDEs.

These resources will be migrated and consolidated to sub-pages of this page soon™️.

- [`#jetbrains-ide` internal Slack channel](https://gitlab.slack.com/archives/CR08PTQ6T)
- [Chad Woolley's Jetbrains IDE setup notes](https://gitlab.com/cwoolley-gitlab/cwoolley-gitlab/-/blob/main/gitlab-workstation-setup-notes.md#jetbrains-ide-setup)
- [Chad Woolley's curated list of Jetbrains overridden settings](https://github.com/thewoolleyman/workstation/blob/master/README.md#jetbrains-overridden-settings)
