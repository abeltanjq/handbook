---
title: "Senior Director of Pipe-to-Spend"
---

## Responsibilities

- The Senior Director of Pipe-to-Spend will own online pipe-to-spend, marketing operations, marketing program management, and field marketing.
- Ensure enough inbound demand is generated to ensure the inbound lead qualification team hits their sales accepted opportunity target.
- Support sales, sales development, and channel marketing in hitting their sales pipeline creation targets.
- Exhibit deep knowledge of software marketing, as well as people management experience.
- Metrics-based approach to marketing planning and strategy.

## Requirements

- At least 4 years marketing experience in a software context
- At least 2 years experience managing people
- At least 2 years experience with content marketing
- At least 2 years experience with field marketing
- Experience with lead scoring & nurturing models
- Experience with driving direct sales in an online/SaaS context
- Experience with Pardot, Marketo, Hubspot or similar tools
- Experience with Google Analytics, MixPanel, and content optimization tools
- Experience running SEM campaigns and other paid traffic
- Experience with Social Media Marketing
- Robust understanding of marketing funnels & metrics
- Experience with Account-based Marketing
- Stays on top of trends in the space via conferences, publications, social media, etc.
- You share our [values](/handbook/values/), and work in accordance with those values.
- [Leadership at GitLab](https://about.gitlab.com/company/team/structure/#director-group)
- Ability to use GitLab

## Nice-to-haves

- Experience with remote-friendly, remote-mostly or remote-only work cultures
- Experience at multiple SaaS companies
- Direct experience in the Developer Tools or Collaboration spaces e.g. Atlassian, GitHub, JetBrains, Slack, Xamarin, New Relic, etc.
- Marketing experience beyond demandgen & leadgen
